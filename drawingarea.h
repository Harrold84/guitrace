#ifndef DRAWINGAREA_H
#define DRAWINGAREA_H

#include <QColor>
#include <QPoint>
#include <QWidget>
#include <QMouseEvent>
#include <QPainter>

#include <QTime>
#include <QTimer>

#include <QDebug>


class DrawingArea : public QWidget
{
    Q_OBJECT
public:
    DrawingArea(QWidget *parent=0);
    ~DrawingArea();

    void setPenColor(const QColor &newColor);
    void setPenWidth(int newWidth);

    QColor getPenColor() const { return penColor; }
    int getPenWidth() const { return drawWidth; }
    QSize sizeHint() const;

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent *event);

private:
    void drawLineTo(const QPoint &endPoint);
    /* determine what to use for the resize object*/
    void resizeObject();
    void resizeImage(QImage *image, const QSize &newSize);

    bool drawing;
    int drawWidth;
    QColor penColor;
    QPoint lastPoint;
    QImage image;

    int parentWidth, parentHeight;
};

#endif // DRAWINGAREA_H
