#include "drawingarea.h"


DrawingArea::DrawingArea(QWidget *parent) : QWidget(parent, Qt::FramelessWindowHint | Qt::WindowSystemMenuHint | Qt::WindowStaysOnTopHint)
{
    //setAttribute(Qt::WA_StaticContents);
    setAutoFillBackground(true);
   // setAttribute(Qt::WA_NoSystemBackground);
    setAttribute(Qt::WA_TranslucentBackground);
   // setWindowOpacity(100.0);
   // setAttribute(Qt::WA_PaintOnScreen);


    // set widget background cvolor
    qDebug() << "DrawingArea created!";
    setBackgroundRole(QPalette::Window);
    QPalette p(palette());
    p.setColor(QPalette::Window, Qt::transparent);
    setPalette(p);


    drawing = false;
    drawWidth = 1;
    penColor = Qt::blue;
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start();
    QImage image2(800,600, QImage::Format_ARGB32);
    image = image2;
    image.fill(qRgba(0,0,0,0));
  //  image.createAlphaMask(Qt::ColorOnly);
    image = image.createMaskFromColor(QColor(128,64,128).rgba(), Qt::MaskOutColor);

   // parentHeight = parent->geometry().height();
   // parentWidth = parent->geometry().width();v

   //qDebug() << parentHeight << " " << parentWidth;
}

DrawingArea::~DrawingArea()
{

}

void DrawingArea::setPenColor(const QColor &newColor)
{
    penColor = newColor;
}

void DrawingArea::setPenWidth(int newWidth)
{
    drawWidth = newWidth;
}

void DrawingArea::mousePressEvent(QMouseEvent *event)
{
    if( event->button() == Qt::LeftButton)
    {
        lastPoint = event->pos();
        qDebug() << "LastPoint: " << event->pos().x();
        drawing = true;
    }
}

void DrawingArea::mouseMoveEvent(QMouseEvent *event)
{
    if(( event->buttons() & Qt::LeftButton) && drawing)
        drawLineTo(event->pos());
}

void DrawingArea::mouseReleaseEvent(QMouseEvent *event)
{
    if( event->button() == Qt::LeftButton && drawing)
    {
        drawLineTo(event->pos());
        drawing = false;
    }

}

void DrawingArea::paintEvent(QPaintEvent *event)
{

    QPainter painter(this);
    QRect dirtyRect = event->rect();
    painter.drawImage(dirtyRect, image, dirtyRect);
}

void DrawingArea::resizeEvent(QResizeEvent *event)
{
    int side = qMin(width(), height());
    if (width() > image.width() || height() > image.height()) {
        int newWidth = qMax(width() + 128, image.width());
        int newHeight = qMax(height() + 128, image.height());
        qDebug() << "qmax(height() +128, image.height()" << newHeight << " image height " << image.height();
       // side = qMin(newWidth, newHeight);
      //  QRegion maskedRegion(newWidth , newHeight,
              //                    side, QRegion::Rectangle);
       //setMask(maskedRegion);
      //  image = image.createMaskFromColor(QColor(128,64,128, 0).rgba(), Qt::MaskOutColor);
      //  setMask(maskedRegion);
        resizeImage(&image, QSize(newWidth, newHeight));
        update();
    }
  /*  int side = qMin(width(), height());
    QRegion maskedRegion(width() / 2 - side / 2, height() / 2 - side / 2, side,
                              side, QRegion::Rectangle);
    setMask(maskedRegion);
    update();*/
    QWidget::resizeEvent(event);
}

void DrawingArea::drawLineTo(const QPoint &endPoint)
{
    QPainter painter(&image);
    painter.setPen(QPen(penColor, drawWidth, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
    painter.drawLine(lastPoint, endPoint);
    int rad = (drawWidth / 2) + 2;
    update(QRect(lastPoint, endPoint).normalized().adjusted(-rad, -rad, +rad, +rad));
    lastPoint = endPoint;
}

void DrawingArea::resizeImage(QImage *image, const QSize &newSize)
{
    if (image->size() == newSize)
        return;

    QImage newImage(newSize, QImage::Format_ARGB32);
    qDebug() << "Alpha: " << newImage.hasAlphaChannel();
    newImage.fill(qRgba(0,0,0,0));
    QPainter painter(&newImage);
    painter.drawImage(QPoint(0, 0), *image);
    *image = newImage;
}

QSize DrawingArea::sizeHint() const
{
    return QSize(800, 600);
}
