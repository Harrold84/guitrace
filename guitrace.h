#ifndef GUITRACE_H
#define GUITRACE_H

#include <QMainWindow>
#include <QString>
#include <phonon/MediaObject>
#include <phonon/AudioOutput>
#include <phonon/SeekSlider>
#include <phonon/VideoPlayer>
#include <QMessageBox>
#include <QAction>
#include <QFileDialog>
#include <QDesktopServices>
#include <QListWidget>
#include <QListWidgetItem>
#include <QGraphicsEllipseItem>
#include <QGraphicsView>
#include <QDesktopWidget>
#include <QStandardItemModel>
#include <QMatrix3x3>


#include "drawingarea.h"
#include "drawingitem.h"
#include "Transparent.h"
#include "save.h"

namespace Ui {
class GUITrace;
}


using namespace std;
class GUITrace : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit GUITrace();
    ~GUITrace();

    QString timeConversion(int msec);

    bool LessThan(QListWidgetItem *a, QListWidgetItem *b);
 /*   float distance(Point2f p1, Point2f p2, Mat homography_matrix);
    void convert_to_image_coordinate(const std::vector<Point2f> & points_in_world_coordinate, const Mat & homography_matrix, std::vector<Point2f> * points_in_image_coordinate);
    void convert_to_image_coordinate(const Point2f & point_in_world_coordinate, const Mat & homography_matrix, Point2f * point_in_image_coordinate);
    void convert_to_world_coordinate(const std::vector<Point2f> & points_in_image_coordinate, const Mat & homography_matrix, std::vector<Point2f> * points_in_world_coordinate);
    Point2f QPointToPoint2F(QPointF toPoint2F);
    */
protected:
   // virtual void showEvent(QShowEvent *event);
  //  virtual void moveEvent(QMoveEvent *event);
    virtual void resizeEvent(QResizeEvent *event);
    virtual bool eventFilter(QObject * object, QEvent * event);

  //  void moveTransparent();

signals:
    void drawOrMoveSignal(int value);
    void itemName(QString name);
    void sendVideoPlayerSize(QRect value);
    void sendGender(QString gender);
    void sendHeadOrFoot(QString headOrFoot);
    void sendCircleSize(int size);

private slots:
    void stateChanged(Phonon::State newState, Phonon::State oldState);
    void addFiles();
    void forward();
    void backward();
    void fpsSpinBoxChanged(int value);
    void showTransparentLayer();
    void showVideo();
    void addCoordinates();
    bool addIntoListBox();
    bool addIntoTable();
    void textIsChanged(QString text);
    void playSlot();
    void checkRadioButtons();
    void addPushButtonPressed();
    bool deletePushButtonPressed();
    void videoPushButtonPressed();
    void setItem(QListWidgetItem *item);
    void setDrawToMove();
    void setHeadFootString(QString string);
    void setMaleFemaleString(QString string);
    void storePushButtonPressed();
    void circleSpinBoxChanged(int value);
    
private:
    Ui::GUITrace *ui;
    TransparentWindow *transparent;
    void emitMySignal();
    void emitItemNameSignal();
    void emitVideoPlayerSignal();
    void emitGender();
    void emitHeadOrFoot();
    void emitCircleSizeSignal();

    QString maleOrFemaleString;
    QString headOrFootString;


    QString setVideoSource();
    QString objectNameText;
    int spinBoxValue;
    int circleSpinBoxSize;

    // Custom Actions
    QAction *playAction;
    QAction *stopAction;
    QAction *pauseAction;
    QAction *nextAction;
    QAction *prevAction;
    void setupCustomActions();

    // setup the connections and signals
    void setupConnections();

    // local variables needed to store information from the GUI fields
    int currentFPS;

    DrawingArea *drawingArea;

    int videox, videoy, videoWidth, videoHeight; // get the position of the video player

    // store information of X/Y coordinates in a QPoint List
    QList<QPoint> coordinateList;
    Save *save;

    int drawOrMove;
    QRect videoPlayerSize;

    // variables for deleting the items
    QString deleteItemName;
    QListWidgetItem *deleteItem;

    // boolean value to see if the window is fullscreen or not
    bool isFullScreen;
    Phonon::VideoPlayer *videoPlayer;

    // the item model
    QStandardItemModel *itemModel;

    QString storePushButtonCoordinates;
};

class SortList
{
public:
    SortList();
    bool operator() ( const QListWidgetItem *left, const QListWidgetItem *right);
};


#endif // GUITRACE_H
