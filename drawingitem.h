#ifndef DRAWINGITEM_H
#define DRAWINGITEM_H



#include <QPainter>
#include <QPainterPath>
#include <QRectF>
#include <QtGui/QColor>
#include <QtGui/QGraphicsItem>
#include <QGraphicsSceneMouseEvent>

class DrawingItem : public QGraphicsItem
{

public:
    DrawingItem(const QString &name, qreal posX = 0, qreal posY = 0, int circle = 0, const QColor color = 0);
    //explicit DrawingItem(QGraphicsEllipseItem *anItem = 0);
    //explicit DrawingItem();
    ~DrawingItem();
    
    void setItemCoordinatesF(QPointF point);
    void setItemCoordinates(QPoint point);
    void setItemName(const QString &name);

    QPointF getItemCoordinatesF() { return itemCoordinatesF; }
    QPoint getItemCoordinates() { return itemCoordinates; }
    QPointF getPrevCoordinates() { return prevCoordinates; }
    QString getItemName() { return itemName; }
    void appendInCoordinatesList( qreal x, qreal y );
    QList<QPointF> getCoordinateList() { return coordinatesList; }
    void setHeadCoordinates(QPointF point) { this->headCoordinates = point; }
    QPointF getHeadCoordinates() { return headCoordinates; }

    // reimplemented functions
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    virtual QPainterPath shape() const;
    virtual QRectF boundingRect() const;

    QString getGender() { return maleOrFemale; }
    QString getHeadOrFoot() { return headOrFoot; }

    void setGender(QString gender) { this->maleOrFemale = gender; }
    void setHeadOrFoot(QString headOrFoot)
    {
        if( headOrFoot.contains("Head") )
            head = true;
        else
            head = false;
        this->headOrFoot = headOrFoot;
    }

    bool isHeadTrue() { return head; }

     QString itemName;

signals:
    
public slots:

protected:


    QPointF itemCoordinatesF;
    QPoint itemCoordinates;
    QPolygonF myPolygon;
    QPointF prevCoordinates;
    QPointF headCoordinates;

    // member variables to hold the x/y position
    qreal positionX, positionY;
    QString headOrFoot;
    QString maleOrFemale;
    int circle;
    bool head;

    QList<QPointF> coordinatesList;
    QColor color;
};

#endif // DRAWINGITEM_H
