#include "guitrace.h"
#include "ui_guitrace.h"

GUITrace::GUITrace() :

    ui(new Ui::GUITrace)
{
    ui->setupUi(this);
    currentFPS = 0;

    /** for testing do some drawing **/
    transparent = new TransparentWindow(0,0,0,0,0);

    this->videoPlayer = new Phonon::VideoPlayer(Phonon::VideoCategory);
    this->videoPlayer->setWindowFlags(Qt::FramelessWindowHint);

    // videoPlayer->setGeometry(0,0,desk.width(), desk.height());
    ui->seekSlider->setMediaObject(videoPlayer->mediaObject());
    ui->seekSlider->setSingleStep(1000/30);
    ui->seekSlider->setPageStep(1000/30);



    ui->drawRadioButton->setChecked(true);

    setupCustomActions();
    setupConnections();

    save = new Save();

    spinBoxValue = ui->fpsSpinBox->value();
    ui->drawComboBox->setCurrentIndex(-1);
    ui->headFootComboBox->setCurrentIndex(-1);
    ui->genderComboBox->setCurrentIndex(-1);

    circleSpinBoxSize = ui->circleSizeSpinBox->value();

    //setup the itemmodel for the table view
    itemModel = new QStandardItemModel(this);
    itemModel->setHorizontalHeaderItem(0, new QStandardItem(QString("Name")));
    itemModel->setHorizontalHeaderItem(1, new QStandardItem(QString("Gender")));
    itemModel->setHorizontalHeaderItem(2, new QStandardItem(QString("Head/Foot")));
    itemModel->setHorizontalHeaderItem(3, new QStandardItem(QString("Timeline")));
    ui->tableView->setModel(itemModel);

    drawOrMove = 0;
    isFullScreen = false;
    transparent->installEventFilter(this);
    installEventFilter(this);
    storePushButtonCoordinates = "";
    //  transparent->show();
}

GUITrace::~GUITrace()
{
    delete save;
    delete transparent;
    delete ui;
}

void GUITrace::setupCustomActions()
{
    playAction = new QAction(style()->standardIcon(QStyle::SP_MediaPlay), tr("Play"), this);
    playAction->setShortcut(tr("Ctrl+P"));
    ui->playButton->setIcon(style()->standardIcon(QStyle::SP_MediaPlay));
    ui->playButton->addAction(playAction);

    pauseAction = new QAction(style()->standardIcon(QStyle::SP_MediaPause), tr("Pause"), this);
    pauseAction->setShortcut(tr("Ctrl+A"));
    ui->pauseButton->setIcon(style()->standardIcon(QStyle::SP_MediaPause));
    ui->pauseButton->addAction(pauseAction);

    stopAction = new QAction(style()->standardIcon(QStyle::SP_MediaStop), tr("Stop"), this);
    stopAction->setShortcut(tr("Ctrl+S"));
    ui->stopButton->setIcon(style()->standardIcon(QStyle::SP_MediaStop));
    ui->stopButton->addAction(stopAction);

    nextAction = new QAction(style()->standardIcon(QStyle::SP_MediaSkipForward), tr("Next"), this);
    nextAction->setShortcut(tr("Ctrl+N"));
    ui->nextButton->setIcon(style()->standardIcon(QStyle::SP_MediaSkipForward));
    ui->nextButton->addAction(nextAction);

    prevAction = new QAction(style()->standardIcon(QStyle::SP_MediaSkipBackward), tr("Previous"), this);
    prevAction->setShortcut(tr("Ctrl+R"));
    ui->previousButton->setIcon(style()->standardIcon(QStyle::SP_MediaSkipBackward));
    ui->previousButton->addAction(prevAction);



}

void GUITrace::stateChanged(Phonon::State newState, Phonon::State oldState)
{
    switch (newState) {
    case Phonon::ErrorState:
        if (videoPlayer->mediaObject()->errorType() == Phonon::FatalError) {
            QMessageBox::warning(this, tr("Fatal Error"),
                                 videoPlayer->mediaObject()->errorString());
        } else {
            QMessageBox::warning(this, tr("Error"),
                                 videoPlayer->mediaObject()->errorString());
        }
        break;
    case Phonon::PlayingState:
        playAction->setEnabled(false);
        pauseAction->setEnabled(true);
        stopAction->setEnabled(true);
        break;
    case Phonon::StoppedState:
        stopAction->setEnabled(false);
        playAction->setEnabled(true);
        pauseAction->setEnabled(false);
        break;
    case Phonon::PausedState:
        pauseAction->setEnabled(false);
        stopAction->setEnabled(true);
        playAction->setEnabled(true);
        break;
    case Phonon::BufferingState:
        break;
    default:
        ;
    }
}

void GUITrace::addFiles()
{
    transparent->hide();
    QString file = QFileDialog::getOpenFileName(this, tr("Select Video File"), QDesktopServices::storageLocation(QDesktopServices::MoviesLocation), tr("Video Files (*.avi)"));
    if( file.isEmpty() )
        return;

    videoPlayer->mediaObject()->setCurrentSource(file);
    videoPlayer->mediaObject()->pause();
}

void GUITrace::setupConnections()
{

    connect(ui->actionExit, SIGNAL(triggered()), transparent, SLOT(close()));
    connect(ui->actionExit, SIGNAL(triggered()), videoPlayer, SLOT(close()));

    connect(videoPlayer, SIGNAL(finished()), videoPlayer, SLOT(deleteLater()));

    connect(videoPlayer->mediaObject(), SIGNAL(stateChanged(Phonon::State,Phonon::State)),
            this, SLOT(stateChanged(Phonon::State,Phonon::State)));

    // connect the buttons to the actions
    connect(ui->playButton, SIGNAL(clicked()), playAction, SLOT(trigger()));
    connect(ui->stopButton, SIGNAL(clicked()), stopAction, SLOT(trigger()));
    connect(ui->pauseButton, SIGNAL(clicked()), pauseAction, SLOT(trigger()));
    connect(ui->nextButton, SIGNAL(clicked()), nextAction, SLOT(trigger()));
    connect(ui->previousButton, SIGNAL(clicked()), prevAction, SLOT(trigger()));

    //setup the mediaobject connections
    //connect(playAction, SIGNAL(triggered()), videoPlayer->mediaObject(), SLOT(play()));
    connect(playAction, SIGNAL(triggered()), this, SLOT(showTransparentLayer()));
    connect(playAction, SIGNAL(triggered()), this, SLOT(showVideo()));
    connect(playAction, SIGNAL(triggered()), this, SLOT(playSlot()));
    connect(pauseAction, SIGNAL(triggered()), videoPlayer->mediaObject(), SLOT(pause()) );
    connect(stopAction, SIGNAL(triggered()), videoPlayer->mediaObject(), SLOT(stop()));
    connect(nextAction, SIGNAL(triggered()), this, SLOT(forward()));
    connect(nextAction, SIGNAL(triggered()), this, SLOT(addCoordinates()));
    connect(nextAction, SIGNAL(triggered()), this, SLOT(addIntoTable()));
    //connect(nextAction, SIGNAL(triggered()), this, SLOT(addIntoListBox()));
    connect(prevAction, SIGNAL(triggered()), this, SLOT(backward()));

    connect(this, SIGNAL(destroyed()), transparent, SLOT(close()));
    connect(this, SIGNAL(destroyed()), videoPlayer, SLOT(close()));

    // connect the UI fields
    connect(ui->fpsSpinBox, SIGNAL(valueChanged(int)), this, SLOT(fpsSpinBoxChanged(int)));
    connect(this, SIGNAL(itemName(QString)), transparent->transparentScene(), SLOT(setItemName(QString)));
    connect(this, SIGNAL(sendGender(QString)), transparent->transparentScene(), SLOT(setGender(QString)));
    connect(this, SIGNAL(sendHeadOrFoot(QString)), transparent->transparentScene(), SLOT(setHeadFoot(QString)));

    connect(ui->circleSizeSpinBox, SIGNAL(valueChanged(int)), this, SLOT(circleSpinBoxChanged(int)));
    connect(this, SIGNAL(sendCircleSize(int)), transparent->transparentScene(), SLOT(getCircleSize(int)));

    connect(ui->drawComboBox, SIGNAL(currentIndexChanged(QString)), transparent->transparentScene(), SLOT(DrawComboBoxSlot(QString)));
    connect(ui->objectNameLineEdit, SIGNAL(textChanged(QString)), ui->objectNameLineEdit, SLOT(setText(QString)));
    connect(ui->objectNameLineEdit, SIGNAL(textEdited(QString)), this, SLOT(textIsChanged(QString)));
    connect(ui->genderComboBox, SIGNAL(currentIndexChanged(QString)), ui->genderComboBox, SLOT(setEditText(QString)));
    connect(ui->headFootComboBox, SIGNAL(currentIndexChanged(QString)), ui->headFootComboBox, SLOT(setEditText(QString)));

    connect(ui->genderComboBox, SIGNAL(currentIndexChanged(QString)), this, SLOT(setMaleFemaleString(QString)));
    connect(ui->headFootComboBox, SIGNAL(currentIndexChanged(QString)), this, SLOT(setHeadFootString(QString)));

    connect(ui->drawRadioButton, SIGNAL(clicked()), this, SLOT(checkRadioButtons()));
    connect(ui->moveRadioButton, SIGNAL(clicked()), this, SLOT(checkRadioButtons()));
    connect(transparent->transparentScene(), SIGNAL(emitRadioButtonChangeSignal()), this, SLOT(setDrawToMove()));


    connect(this, SIGNAL(drawOrMoveSignal(int)), transparent->transparentScene(), SLOT(getDrawOrMove(int)));
    //  connect(ui->insertPushButton, SIGNAL(clicked()), this, SLOT(addIntoListBox()));
    connect(ui->insertPushButton, SIGNAL(clicked()), this, SLOT(addPushButtonPressed()));
    connect(this, SIGNAL(sendVideoPlayerSize(QRect)), transparent->transparentScene(), SLOT(getVideoPlayerSize(QRect)));
    connect(ui->objectListWidget, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(setItem(QListWidgetItem*)));
    connect(ui->objectListWidget, SIGNAL(itemChanged(QListWidgetItem*)), this, SLOT(setItem(QListWidgetItem*)));
    connect(ui->objectListWidget, SIGNAL(currentTextChanged(QString)), this, SLOT(textIsChanged(QString)));
    connect(ui->removeObjectName, SIGNAL(clicked()), this, SLOT(deletePushButtonPressed()));
    connect(videoPlayer->mediaObject(), SIGNAL(finished()), transparent, SLOT(close()));

    connect(transparent->transparentScene(), SIGNAL(sceneRectChanged(QRectF)), transparent->transparentScene(), SLOT(update(QRectF)));

    connect(ui->storePushButton, SIGNAL(clicked()), this, SLOT(storePushButtonPressed()));

}

void GUITrace::playSlot()
{
    ui->fpsSpinBox->setValue(15);
    videoPlayer->mediaObject()->play(); // play the video
    // pause the video instantly
    videoPlayer->mediaObject()->pause();
    qDebug() << "geometry: " << videoPlayer->geometry();
    videox = videoPlayer->x();
    videoy = videoPlayer->y();
    videoWidth = videoPlayer->width();
    videoHeight = videoPlayer->height();

    videoPlayerSize.setRect(videox, videoy, videoWidth, videoHeight);
    emitVideoPlayerSignal();
}

void GUITrace::setHeadFootString(QString string)
{
    this->headOrFootString = string;
}

void GUITrace::setMaleFemaleString(QString string)
{
    this->maleOrFemaleString = string;
}

void GUITrace::textIsChanged(QString text)
{
    objectNameText = text;
}

// seek by 1 second / 30
void GUITrace::forward()
{
    ui->seekSlider->mediaObject()->seek(ui->seekSlider->mediaObject()->currentTime() + spinBoxValue);
}

void GUITrace::backward()
{
    ui->seekSlider->mediaObject()->seek(ui->seekSlider->mediaObject()->currentTime() - spinBoxValue);
}

void GUITrace::fpsSpinBoxChanged(int value)
{
    value = ui->fpsSpinBox->value();
    ui->seekSlider->setSingleStep((value*1000)/30);
    ui->seekSlider->setPageStep((value*1000)/30);
    spinBoxValue = ui->seekSlider->singleStep();
}

void GUITrace::circleSpinBoxChanged(int value)
{
    value = ui->circleSizeSpinBox->value();
    circleSpinBoxSize = value;
    emitCircleSizeSignal();
}

void GUITrace::resizeEvent(QResizeEvent *event)
{
    //   qDebug() << videox << videoy << videoWidth << videoHeight << "GGGNNNN";

    // transparent->setGeometry(videox, videoy, videoWidth, videoHeight);
    qDebug() << videoPlayer->geometry() << "player geom";
    //videoPlayer->resize(transparent->transparentScene()->returnSetVideoPlayerSize().width(), transparent->transparentScene()->returnSetVideoPlayerSize().height());
    videoPlayer->setGeometry(0,0, 1020,500);
    videox = videoPlayer->x();
    videoy = videoPlayer->y();
    videoWidth = videoPlayer->width();
    videoHeight = videoPlayer->height();
    // send the size to the transparent object
    videoPlayerSize.setRect(videox, videoy, videoWidth, videoHeight);
    emitVideoPlayerSignal();

    transparent->setGeometry(videox, videoy, 1020, 500);
    // transparent->getTransparentWindowView()->sets

    // transparent->setViewSize();
    // load the transparent object

    // set the fullScreen flag to true
    this->isFullScreen = true;

    QMainWindow::resizeEvent(event);
    // moveTransparent();
}

void GUITrace::addCoordinates()
{
    int incrementValue = 0;
    foreach(DrawingItem* item, transparent->transparentScene()->returnItemList())
    {



        // if the boolean head value is true AND the UI field is set to foot, add extra information into the saved file
        if( ui->headFootComboBox->itemText(ui->headFootComboBox->currentIndex()).contains("Foot") )
        {
            QString extraInformation = "";
            QString nameLineEditText = item->getItemName();
            QString timeValue = timeConversion(ui->seekSlider->mediaObject()->currentTime());
            QString xValue = QString::number(item->getItemCoordinatesF().x());
            QString yValue = QString::number(item->getItemCoordinatesF().y());
            QString headValueX = QString::number(item->getHeadCoordinates().x());
            QString headValueY = QString::number(item->getHeadCoordinates().y());
            if( headValueX == "0" && headValueY == "0" )
            {
                extraInformation.clear();
            }
            else
            {
                extraInformation = QString(" Set to foot at coordinates: %1x%2 ").arg(xValue).arg(yValue);
                yValue.append(" Set to head coordinates at: ").append(QString("%1x%2").arg(headValueX).arg(headValueY));
            }

            if( item->isHeadTrue() == true )
            {

                save->saveInformation(timeValue, nameLineEditText, item->getGender(), item->getHeadOrFoot(),
                                      xValue, yValue, extraInformation);
            }
            incrementValue += 1;
        }
        else
        {
            QString extraInformation = "";
            QString nameLineEditText = item->getItemName();
            QString timeValue = timeConversion(ui->seekSlider->mediaObject()->currentTime());
            QString xValue = QString::number(item->getItemCoordinatesF().x());
            QString yValue = QString::number(item->getItemCoordinatesF().y());


            if( item->isHeadTrue() == true )
            {

                save->saveInformation(timeValue, nameLineEditText, item->getGender(), item->getHeadOrFoot(),
                                      xValue, yValue, extraInformation);
            }
        }
    }

    ui->headFootComboBox->setCurrentIndex(0);
}



bool GUITrace::LessThan(QListWidgetItem *a, QListWidgetItem *b)
{
    return *a < *b;
}

// store the current information (x/y coordinates) in an extra set of variables
void GUITrace::storePushButtonPressed()
{
    QList<QListWidgetItem*> selectedItem = ui->objectListWidget->selectedItems();
    QList<DrawingItem*> tempList;
    // qSort(selectedItem.begin(), selectedItem.end(), LessThan);
    qSort(selectedItem.begin(), selectedItem.end(), SortList());
    storePushButtonCoordinates.clear();
    // we only allow ONE item to be selected and stored
    qDebug() << "selected item size; " << selectedItem.size();
    if( selectedItem.size() >= 1 )
    {
        for( int i = 0; i < selectedItem.size(); i++ )
        {
            for( int j = 0; j < transparent->transparentScene()->returnItemList().size(); j++ )
            {

                DrawingItem *item = transparent->transparentScene()->returnItemList().at(j);
                //      qDebug() << "selected item name at: " << selectedItem.at(i)->text();
                //      qDebug() << "item.getItemName(): " << item->getItemName();
                if( item->getItemName().compare( selectedItem.at(i)->text()) == 0 )
                {
                    item->setHeadCoordinates(QPointF(item->getItemCoordinatesF().x(), item->getItemCoordinatesF().y()));
                    qDebug() << "head: " << item->getHeadCoordinates();
                    qDebug() << "itemname: " << item->getItemName();
                    //  storePushButtonCoordinates.append(QString::number(item->getItemCoordinatesF().x())).append(QString('x')).append(QString::number(item->getItemCoordinatesF().y()));
                    //   qDebug() << storePushButtonCoordinates;
                }
            }

        }
        // check the current selected item in the item list if it is available
        /* foreach( DrawingItem* item, transparent->transparentScene()->returnItemList() )
        {
            qDebug() << "Itemname: " << item->getItemName();
            if( item->getItemName().compare( selectedItem.at(0)->text() ) == 0 )
            {
                storePushButtonCoordinates.append(QString::number(item->getItemCoordinatesF().x())).append(QString('x')).append(QString::number(item->getItemCoordinatesF().y()));
                qDebug() << storePushButtonCoordinates;
            }
        }
        */
    }
}

void GUITrace::showTransparentLayer()
{
    if( transparent->isHidden() == true )
    {
        transparent->show();
    }
}

void GUITrace::showVideo()
{
    if( videoPlayer->isHidden() == true )
    {
        videoPlayer->show();
    }
}

bool GUITrace::addIntoListBox()
{
    // check if the item to be inserted already exists
    if( ui->objectListWidget->count() == 0 )
    {
        qDebug() << "objectnametext is: " << objectNameText;
        ui->objectListWidget->addItem(objectNameText);
        return true;
    }
    bool found = false;

    for( int i = 0; i < ui->objectListWidget->count(); i++ )
    {

        QString item = ui->objectListWidget->item(i)->text();
        qDebug() << "item:" << item;
        qDebug() << "text:" << objectNameText;
        if( item == objectNameText)
        {
            // do nothing
            found = false;
        }
        else
        {
            // else insert item
            found = true;
        }

    }
    if( found )
    {
        ui->objectListWidget->addItem(objectNameText);
    }
    return false;
}

bool GUITrace::addIntoTable()
{
        itemModel->clear();

    int rList = transparent->transparentScene()->returnItemList().size();
    foreach( DrawingItem *drawingItem, transparent->transparentScene()->returnItemList() )
    {
        // insert the last old coordinate into the coordinate list
        if( (drawingItem->getPrevCoordinates().x() != 0) && (drawingItem->getPrevCoordinates().y() != 0) )
            drawingItem->appendInCoordinatesList(drawingItem->getPrevCoordinates().x(), drawingItem->getPrevCoordinates().y());

        QStandardItem *nameItem = new QStandardItem();
        QStandardItem *genderItem = new QStandardItem();
        QStandardItem *timeValue = new QStandardItem();
        QStandardItem *headFootItem = new QStandardItem();
        QStandardItem *xItem = new QStandardItem();
        QStandardItem *yItem = new QStandardItem();
        QStandardItem *xyItem = new QStandardItem();
        QList<QStandardItem *> items;
        QList<QStandardItem *> tItem;
        nameItem->setText(QString(drawingItem->getItemName()));
        genderItem->setText(QString(drawingItem->getGender()));
        timeValue->setText(QString(timeConversion(ui->seekSlider->mediaObject()->currentTime())));
        headFootItem->setText(QString(drawingItem->getHeadOrFoot()));
       // xyItem->setText(QString::number(drawingItem->getItemCoordinatesF().x()).append(QString("x").
         //                                                                              append(QString::number(drawingItem->getItemCoordinatesF().y()))));

        qDebug() << "nameItem: " << nameItem->text();
        items.push_back(nameItem);
        items.push_back(genderItem);
        items.push_back(headFootItem);
        items.push_back(timeValue);
        //items.push_back(xyItem);
        //itemModel->appendRow(items);
        qDebug() << "columncount: " << itemModel->columnCount();

        qDebug() << "Coordinatelist size: " << drawingItem->getCoordinateList().size();

        for( int i = 0; i < drawingItem->getCoordinateList().size(); i++ )
        {
            QStandardItem *cItem = new QStandardItem();
            cItem->setText(QString::number(drawingItem->getCoordinateList().at(i).x()).append(QString("x").
                                                                                               append(QString::number(drawingItem->getCoordinateList().at(i).y()))));
            qDebug() << "citem: " << cItem->text();
            items.push_back(cItem);
           // itemModel->appendColumn(tItem);
        }

        //tItem.clear();
        itemModel->appendRow(items);
        items.clear();
    }

    // foreach item append some information into the table
    /* foreach( DrawingItem *drawingItem, transparent->transparentScene()->returnItemList() )
    {
        // check if an item exists already
        QList<QStandardItem*> findItem = itemModel->findItems(drawingItem->getItemName());
        qDebug() << "findItem size: " << findItem.size();
        // an extra case if the QList is empty
        qDebug() << "Drawingitem: " << drawingItem->getItemName();
       /* if( findItem.size() <= 0 )
        {
            QList<QStandardItem *> items;
            nameItem->setText(QString(drawingItem->getItemName()));
            genderItem->setText(QString(drawingItem->getGender()));
            timeValue->setText(QString(timeConversion(ui->seekSlider->mediaObject()->currentTime())));
            headFootItem->setText(QString(drawingItem->getHeadOrFoot()));
            //    xItem->setText(QString::number(drawingItem->getItemCoordinatesF().x()));
            //    yItem->setText(QString::number(drawingItem->getItemCoordinatesF().y()));
                xyItem->setText(QString::number(drawingItem->getItemCoordinatesF().x()).append(QString("x").
                                                                                               append(QString::number(drawingItem->getItemCoordinatesF().y()))));


            items.push_back(nameItem);
            items.push_back(genderItem);
            items.push_back(headFootItem);
            items.push_back(timeValue);
            items.push_back(xyItem);
            itemModel->appendRow(items);
        }
        foreach(QStandardItem* fItem, findItem)
        {
            if( fItem->text().contains(QString(drawingItem->getItemName())))
            {
                continue;
            }
            else
            {
                QList<QStandardItem *> items;
                nameItem->setText(QString(drawingItem->getItemName()));
                genderItem->setText(QString(drawingItem->getGender()));
                timeValue->setText(QString(timeConversion(ui->seekSlider->mediaObject()->currentTime())));
                headFootItem->setText(QString(drawingItem->getHeadOrFoot()));
            //    xItem->setText(QString::number(drawingItem->getItemCoordinatesF().x()));
            //    yItem->setText(QString::number(drawingItem->getItemCoordinatesF().y()));
                xyItem->setText(QString::number(drawingItem->getItemCoordinatesF().x()).append(QString("x").
                                                                                               append(QString::number(drawingItem->getItemCoordinatesF().y()))));

                items.push_back(nameItem);
                items.push_back(genderItem);
                items.push_back(headFootItem);
                items.push_back(timeValue);
                items.push_back(xyItem);
                itemModel->appendRow(items);
            }
        }
    }*/
    return true;
}

// this function is taken from user "luggi" from http://qt-project.org/forums/viewthread/5879
QString GUITrace::timeConversion(int msecs)
{
    QString formattedTime;

    int hours = msecs/(1000*60*60);
    int minutes = (msecs-(hours*1000*60*60))/(1000*60);
    int seconds = (msecs-(minutes*1000*60)-(hours*1000*60*60))/1000;
    int milliseconds = msecs-(seconds*1000)-(minutes*1000*60)-(hours*1000*60*60);

    formattedTime.append(QString("%1").arg(hours, 2, 10, QLatin1Char('0')) + ":" +
                         QString( "%1" ).arg(minutes, 2, 10, QLatin1Char('0')) + ":" +
                         QString( "%1" ).arg(seconds, 2, 10, QLatin1Char('0')) + ":" +
                         QString( "%1" ).arg(milliseconds, 3, 10, QLatin1Char('0')));

    return formattedTime;
}

void GUITrace::checkRadioButtons()
{
    if( ui->drawRadioButton->isChecked() )
    {
        drawOrMove = 1;
        emitMySignal();
    }
    if( ui->moveRadioButton->isChecked() )
    {
        drawOrMove = 2;
        emitMySignal();
    }
}

void GUITrace::addPushButtonPressed()
{
    if( ui->objectNameLineEdit->text().isEmpty() )
    {
        QMessageBox::warning(this, "Add some text", "You have to insert some text first", QMessageBox::Ok);
    }
    else
    {
        addIntoListBox();
        emitItemNameSignal();
        emitGender();
        emitHeadOrFoot();
        ui->objectListWidget->sortItems(Qt::AscendingOrder);
    }
}


bool GUITrace::deletePushButtonPressed()
{
    // get the selected item -- fixed, deleteItemName is the item
    QString selectedItemName = deleteItemName;

    // remove the item from the list shown in the UI
    ui->objectListWidget->takeItem(ui->objectListWidget->row(ui->objectListWidget->currentItem()));
    if( ui->objectListWidget->size().isEmpty() )
    {
        return false;
    }
    // remove the item from the itemList AND the mapList in the TransparentScene class
    /// DEBUGGING
    qDebug() << "GNAH: " << selectedItemName;
    qDebug() << "Size: " << transparent->transparentScene()->returnItemList().size();
    for( int i = 0; i < transparent->transparentScene()->returnItemList().size(); i++ )
    {
        DrawingItem* currentItem = transparent->transparentScene()->returnItemList().at(i);
        qDebug() << "currentitem: " << currentItem;
        if( currentItem->getItemName() == selectedItemName )
        {
            transparent->transparentScene()->removeFromList(i);
            transparent->transparentScene()->removeItem(currentItem);
            return true;
        }
    }

    // sort the items alphabetically
    ui->objectListWidget->sortItems(Qt::AscendingOrder);
    return false;
}

void GUITrace::videoPushButtonPressed()
{
    // videoPlayer->showMaximized();
}

void GUITrace::setItem(QListWidgetItem *item)
{
    deleteItem = item;
    // get the name of the item
    deleteItemName = item->text();
    ui->objectListWidget->setCurrentItem(item);
}

void GUITrace::emitMySignal()
{
    emit drawOrMoveSignal(drawOrMove);
}

void GUITrace::emitItemNameSignal()
{
    qDebug() << "text is: " << objectNameText;
    emit itemName(objectNameText);
}

void GUITrace::emitVideoPlayerSignal()
{
    emit sendVideoPlayerSize(videoPlayerSize);
}

void GUITrace::emitGender()
{
    emit sendGender(ui->genderComboBox->itemText(ui->genderComboBox->currentIndex()));
}

void GUITrace::emitHeadOrFoot()
{
    emit sendHeadOrFoot(ui->headFootComboBox->itemText(ui->headFootComboBox->currentIndex()));
}

void GUITrace::emitCircleSizeSignal()
{
    emit sendCircleSize(circleSpinBoxSize);
}

bool GUITrace::eventFilter(QObject *object, QEvent *event)
{

    if( object == this && event->type() == QEvent::KeyPress )
    {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        if( keyEvent->key() == Qt::Key_F )
        {
            if( !isFullScreen )
            {
                // set the player and the drawing area to fullscreen
                // videoPlayer->showMaximized();

                isFullScreen = true;
                return true;
            }
            else
            {
                // set the player and drawing area to a currently unknown size
                isFullScreen = false;
                return false;
            }
        }
    }

    if( object == transparent && event->type() == QEvent::KeyPress)
    {
        QKeyEvent *keyEvent = static_cast<QKeyEvent *>(event);
        if( keyEvent->key() == Qt::Key_Space )
        {
            videoPlayer->mediaObject()->pause();
        }
    }
    return false;
}

void GUITrace::setDrawToMove()
{
    // if the draw radio button is check, unset it and select the MoveRadioButton
    if( ui->drawRadioButton->isChecked() )
    {
        ui->drawRadioButton->setChecked(false); // unset
        ui->moveRadioButton->setChecked(true); // set
        checkRadioButtons();
    }
}

/*
float GUITrace::distance(Point2f point1, Point2f point2, Mat homography_matrix)
{

    return 0.0f;
}

void GUITrace::convert_to_world_coordinate(const std::vector<Point2f> & points_in_image_coordinate, const Mat & homography_matrix, std::vector<Point2f> * points_in_world_coordinate)
{
    points_in_world_coordinate->clear();
    points_in_world_coordinate->resize(points_in_image_coordinate.size());

    Mat points_in_image_coordinate_mat(points_in_image_coordinate, false); // sharing data
    Mat points_in_world_coordinate_mat(*points_in_world_coordinate, false); // sharing data so output is written to the right place
    perspectiveTransform(points_in_image_coordinate_mat, points_in_world_coordinate_mat, homography_matrix);

}

void GUITrace::convert_to_image_coordinate(const Point2f &point_in_world_coordinate, const Mat &homography_matrix, Point2f *point_in_image_coordinate)
{
    vector<Point2f> world, image;
    world.push_back(point_in_world_coordinate);
    image.push_back(*point_in_image_coordinate);

    convert_to_image_coordinate(world, homography_matrix, &image);

    *point_in_image_coordinate = image[0];

}

void GUITrace::convert_to_image_coordinate(const std::vector<Point2f> & points_in_world_coordinate, const Mat & homography_matrix, std::vector<Point2f> * points_in_image_coordinate)
{
    points_in_image_coordinate->clear();
    points_in_image_coordinate->resize(points_in_world_coordinate.size());

    Mat points_in_world_coordinate_mat(points_in_world_coordinate, false); // sharing data
    Mat points_in_image_coordinate_mat(*points_in_image_coordinate, false); // sharing data so output is written to the right place

    perspectiveTransform(points_in_world_coordinate_mat, points_in_image_coordinate_mat, homography_matrix.inv());

}

Point2f GUITrace::QPointToPoint2F(QPointF toPoint2F)
{
    Point2f retPoint;
    retPoint.x = toPoint2F.x();
    retPoint.y = toPoint2F.y();
    return retPoint;
}
*/

/// NEW CLASS ///
SortList::SortList()
{

}

bool SortList::operator ()(const QListWidgetItem *left, const QListWidgetItem *right)
{
    return *left < *right;
}
