#include "NodeItem.h"
#include <QtGui>

NodeItem::NodeItem(const QString &name, int x, int y)
{
	this->setPos(x, y);
    this->color = Qt::blue;
	setZValue(1);
	setFlags(ItemIsSelectable | ItemIsMovable);
	setAcceptsHoverEvents(true);
}

QRectF NodeItem::boundingRect() const
{
	return QRectF(0, 0, 20, 20);
}

QPainterPath NodeItem::shape() const
{
	QPainterPath path;
	path.addEllipse(0, 0, 20, 20);
	return path;
}

void NodeItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	Q_UNUSED(widget);
	painter->setBrush(QBrush(QColor(0,0,0,64)));
	painter->drawEllipse(0,0,20,20);
}

void NodeItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	QGraphicsItem::mousePressEvent(event);
	update();
}

void NodeItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
	if (event->modifiers() & Qt::ShiftModifier) {
		update();
		return;
	}
	QGraphicsItem::mouseMoveEvent(event);
}

void NodeItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	QGraphicsItem::mouseReleaseEvent(event);
	update();
}
