#ifndef NODE_ITEM_H
#define NODE_ITEM_H

#include <QtGui/QColor>
#include <QtGui/QGraphicsItem>

class NodeItem : public QGraphicsItem
{
public:
	NodeItem(const QString &name, int x, int y);

	QRectF boundingRect() const;
	QPainterPath shape() const;
	void paint(QPainter *painter, const QStyleOptionGraphicsItem *item, QWidget *widget);

protected:
	void mousePressEvent(QGraphicsSceneMouseEvent *event);
	void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
	void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

private:
    QColor color;
};

#endif // NODE_ITEM_H
