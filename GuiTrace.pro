#-------------------------------------------------
#
# Project created by QtCreator 2012-06-06T15:12:02
#
#-------------------------------------------------

QT       += core gui phonon

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GuiTrace
TEMPLATE = app


SOURCES += main.cpp\
        guitrace.cpp \
    session.cpp \
    drawingarea.cpp \
    Transparent.cpp \
    save.cpp \
    drawingitem.cpp \
    NodeItem.cpp

HEADERS  += guitrace.h \
    session.h \
    drawingarea.h \
    Transparent.h \
    save.h \
    drawingitem.h \
    NodeItem.h

FORMS    += guitrace.ui

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../../opencv/build/x86/mingw/lib/
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../../opencv/build/x86/mingw/lib/
else:unix:!macx:!symbian: LIBS += -L$$PWD/../../../../../opencv/build/x86/mingw/lib/

INCLUDEPATH += $$PWD/../../../../../opencv/build/include
INCLUDEPATH += $$PWD/../../../../../opencv/build/include/opencv
DEPENDPATH += $$PWD/../../../../../opencv/build/include
