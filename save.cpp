#include "save.h"

//----------------------------------------------------------------------------//
// Constructors                                              Save             //
//----------------------------------------------------------------------------//

Save::Save()
{
}

//----------------------------------------------------------------------------//
// Functions                                                 TransparentScene //
//----------------------------------------------------------------------------//

// incomplete, read the files manually for now
void Save::loadInformation( const QString &fileName )
{

    QFile file("Blubb");
    if( file.exists() )
    {
        QSettings settings("Blubb", QSettings::IniFormat);

        settings.beginGroup("ObjectName");
        QString object = settings.value("Name").toString();
        qDebug() << "Name: " << object; // debug output
        settings.endGroup();


        settings.beginGroup("Coordinates");
        QStringList keys = settings.allKeys();
        foreach(const QString &key, keys)
            qDebug() << settings.value(key);
        settings.endGroup();

        QSettings setting2;
        int size = setting2.beginReadArray("Coordinates");
        int tempValue = 0; // stores the value of the last step
        for(int i = 1; i < size; i++) {
            setting2.setArrayIndex(i);
            QPoint point;
            QString string;
            //point.setX(settings.value(QString("Step %1").arg(string.toInt())).toInt());
            string = settings.value(QString("Step%1").arg(i)).toString();
            qDebug() << "String: " << string;

        }
        setting2.endArray();
    }
    else
    {
        qDebug() << "File does not exist.";
    }
}

void Save::saveInformation(const QString &time, const QString &objectName, const QString &gender, const QString &headorfoot, QString &x, QString &y, QString &headC)
{
    QSettings settings(objectName, QSettings::IniFormat);

    QTextCodec *codec = QTextCodec::codecForName("UTF-8");
    settings.setIniCodec(codec);

    //----------------------------------------------------------------------------//
    // ObjectName Group                                                           //
    //----------------------------------------------------------------------------//
    settings.beginGroup("ObjectName");
    settings.setValue("Name", objectName);
    settings.endGroup();





    //----------------------------------------------------------------------------//
    // Coordinates Group                                                          //
    //----------------------------------------------------------------------------//

    // read out the current information to see at which position we currently are
    //  int size = settings.beginReadArray("Coordinates");
    int size = 1;
        settings.beginGroup("Coordinates");
        QStringList keys = settings.allKeys();
        foreach(const QString &key, keys)
        {
            size = size+1;
        }

        for( int i = 0; i < size; i++ )
        {
            if( !headC.isEmpty() )
                settings.setValue(QString("Step%1").arg(size), QString("%1x%2%3").arg(x).arg(y).arg(headC));
            else
                settings.setValue(QString("Step%1").arg(size), QString("%1x%2").arg(x).arg(y));
        }

        settings.endGroup();
    //----------------------------------------------------------------------------//
    // TimeLine Group                                                             //
    //----------------------------------------------------------------------------//
    int siz = 1;
    settings.beginGroup("TimeLine");
    QStringList times = settings.allKeys();
    foreach(const QString &key, times)
    {
        siz = siz + 1;
    }

    for( int i = 0; i < siz; i++)
    {
        settings.setValue(QString("Time%1").arg(siz), time);
    }
    settings.endGroup();

    //----------------------------------------------------------------------------//
    // Gender Group                                                               //
    //----------------------------------------------------------------------------//

    settings.beginGroup("Gender");
    settings.setValue("Gender", gender);
    settings.endGroup();

    //----------------------------------------------------------------------------//
    // Head or foot Group                                                         //
    //----------------------------------------------------------------------------//

    settings.beginGroup("Position");
    settings.setValue("Position", headorfoot);
    settings.endGroup();


    saveOldCoordinates(x,y);
}

void Save::saveOldCoordinates(QString &x, QString &y)
{
    oldCoordinates.setX(x.toInt());
    oldCoordinates.setY(y.toInt());
}
