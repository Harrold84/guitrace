//----------------------------------------------------------------------------//
// Preambles                                                                  //
//----------------------------------------------------------------------------//

#include <QWidget>
#include <QGraphicsView>
#include <QGridLayout>
#include <QGraphicsSceneMouseEvent>

#include "Transparent.h"




//----------------------------------------------------------------------------//
// Constructors                                              TransparentScene //
//----------------------------------------------------------------------------//

////////////////////////////////////////////////////////////////////////////////
/// <summary> Creates a new transparent window graphics scene. </summary>

TransparentScene::TransparentScene (QObject* parent) : QGraphicsScene (parent)
{
    pressed = false;
    this->value = 1 ;
    this->incrementValue = 0;
    this->setSceneRect(0,0,width(),height());
}



//----------------------------------------------------------------------------//
// Internal                                                  TransparentScene //
//----------------------------------------------------------------------------//

////////////////////////////////////////////////////////////////////////////////
/// <summary> Draws any background of the scene. </summary>

void TransparentScene::drawBackground (QPainter* painter, const QRectF& rect)
{
    // Draw the background
    //QGraphicsScene::drawBackground (painter, rect);
}

////////////////////////////////////////////////////////////////////////////////
/// <summary> Draws any foreground of the scene. </summary>

void TransparentScene::drawForeground (QPainter* painter, const QRectF& rect)
{
    // Draw the foreground
    qDebug() << "RECTRECT: " << rect;
    setVideoPlayerSize = rect;
    qDebug() << "setvideoplayerrect: " << setVideoPlayerSize;
    QGraphicsScene::drawForeground (painter, rect);
    this->painter = painter;
    // Enable scene antialiasing and smooth image transformation
    painter->setRenderHint (QPainter::Antialiasing);
    painter->setRenderHint (QPainter::SmoothPixmapTransform);

    // Do a test paint
    painter->setPen   (Qt::blue);
    painter->setBrush (QBrush (QColor (0, 0, 0, 64)));
    painter->drawRect (0, 0, width(), height());

}



//----------------------------------------------------------------------------//
// Events                                                    TransparentScene //
//----------------------------------------------------------------------------//

////////////////////////////////////////////////////////////////////////////////
/// <summary> Handles the mouse press event. </summary>

void TransparentScene::mousePressEvent (QGraphicsSceneMouseEvent* event)
{
    pressed = true;
    if( event->button() == Qt::LeftButton)
    {
        pressedPoint = event->buttonDownScenePos(Qt::LeftButton);
        qDebug() << "pressedPoint: " << pressedPoint;
        // check if we select an item with the left click
    }

    QGraphicsScene::mousePressEvent(event);
}

////////////////////////////////////////////////////////////////////////////////
/// <summary> Handles the mouse release event. </summary>
//you don't need moveevent at all for that. Store the x and y of the mouse press, then on the mouse release use that and the release x and y to draw your rectangle
void TransparentScene::mouseReleaseEvent (QGraphicsSceneMouseEvent* event)
{
    releasedPoint = event->scenePos();
    if( event->button() == Qt::LeftButton ) {
        if( getCurrentDrawOption() == "Circle" && getCurrentDrawValue() == 1 )
        {
            //DrawingItem *item = new DrawingItem(addEllipse(pressedPoint.x()-(16/2), pressedPoint.y()-(16/2), 16, 16, QPen(Qt::blue), QBrush(Qt::transparent)));
           // QGraphicsEllipseItem *item = addEllipse(pressedPoint.x()-(16/2), pressedPoint.y()-(16/2), 16, 16, QPen(Qt::blue), QBrush(Qt::transparent));

            DrawingItem *item = new DrawingItem(itemName, pressedPoint.x(), pressedPoint.y(), circle(), QColor(rand()&255, rand()&255, rand()&255 )); //addEllipse(pressedPoint.x()-(16/2), pressedPoint.y()-(16/2), 16, 16, QPen(Qt::blue), QBrush(Qt::transparent));
            item->setHeadOrFoot(headOrFoot);
            item->setGender(gender);
            qDebug() << headOrFoot << gender;
            addItem(item);

            // add the items into the list
            if( !itemList.contains(item) )
            {
                if( !itemName.isEmpty() )
                    itemList.append(item);
            }
          //  item->setData(incrementValue, itemName);

            // set the UI field from Draw to Move
            setDrawToMove();
            item->appendInCoordinatesList(pressedPoint.x(), pressedPoint.y());
        }
    }
    pressed = false;

    update();
    qDebug() << "activated";
    QGraphicsScene::mouseReleaseEvent(event);
    qSort(itemList.begin(), itemList.end(), SortString());
    incrementValue++;
}

void TransparentScene::setDrawToMove()
{
    qDebug() << "Setting somethign to move";
    emit emitRadioButtonChangeSignal();
}

/*
QVariant TransparentScene::itemChange(GraphicsItemChange change, const QVariant &value)
{
    if( change == QGraphicsItem::ItemPositionChange)
    {

    }
    return value;
}*/

////////////////////////////////////////////////////////////////////////////////
/// <summary> Handles the mouse move event. </summary>

void TransparentScene::mouseMoveEvent (QGraphicsSceneMouseEvent* event)
{
    // Handle the event
    QGraphicsScene::mouseMoveEvent (event);
    // get the coordinates
    QList<QGraphicsItem*> selectedItem = selectedItems();
    // move only if there is a selected item activated
    if( selectedItem.size() > 0 ) {

        if( event->buttons() & Qt::LeftButton && pressed ) {
            qDebug() << "scene pos: " << event->scenePos();
            QGraphicsItem *item = selectedItem.at(0);

            DrawingItem *drawItem  = static_cast<DrawingItem*>(item); // cast the QGraphicsItem to a DrawingItem, since it's about the same type

            for( int i = 0; i < itemList.size(); i++ ) {
                QString value = drawItem->getItemName();
                foreach(DrawingItem *dItem, returnItemList())
                {

                    QString value2 = dItem->getItemName();
                   // qDebug() << "Value2 is: " << value2;
                    if( value2.compare(value, Qt::CaseInsensitive) == 0 ) // if the values are the same
                    {
                        // if the values are the same, update the values
                        dItem->setItemCoordinatesF(QPointF(event->scenePos()));

                    }
                }
            }
        }

    }
}

////////////////////////////////////////////////////////////////////////////////
/// <summary> Handles the mouse double click event. </summary>

void TransparentScene::mouseDoubleClickEvent (QGraphicsSceneMouseEvent* event)
{
    // Handle the event
    QGraphicsScene::mouseDoubleClickEvent (event);
}

void TransparentScene::setItemName(const QString &name)
{
    this->itemName = name;
}

void TransparentScene::getCircleSize(int size)
{
    this->circleSize = size;
}

//----------------------------------------------------------------------------//
// Functions                                                 TransparentScene //
//----------------------------------------------------------------------------//

void TransparentScene::drawLineTo(const QPoint &endPoint)
{

}

//----------------------------------------------------------------------------//
// SLOTS                                                    TransparentScene  //
//----------------------------------------------------------------------------//

////////////////////////////////////////////////////////////////////////////////
/// <summary> Invoke whenever the DrawComboBox field is changed </summary>

void TransparentScene::DrawComboBoxSlot(const QString &text)
{
    this->drawOption = text;
}

void TransparentScene::getDrawOrMove(int value)
{
    this->value = value;
}

void TransparentScene::setGender(const QString &name)
{
    this->gender = name;
}

void TransparentScene::setHeadFoot(const QString &name)
{
    this->headOrFoot = name;
}

//----------------------------------------------------------------------------//
// Fields                                                   TransparentScene  //
//----------------------------------------------------------------------------//

////////////////////////////////////////////////////////////////////////////////
/// <summary> Invoke this before the mousebutton is released </summary>

QString TransparentScene::getCurrentDrawOption()
{
    return drawOption;
}

void TransparentScene::getVideoPlayerSize(QRect point)
{
    this->videoPlayerSize = point;
}

//----------------------------------------------------------------------------//
// Constructors                                             TransparentWindow //
//----------------------------------------------------------------------------//

////////////////////////////////////////////////////////////////////////////////
/// <summary> Creates a new transparent window example. </summary>

TransparentWindow::TransparentWindow (QWidget *parent)
    : QWidget (parent, Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint)
{
    // Create a new central widget
    mWidgetMain = new QWidget (this);

    // Create a new graphics view and scene
    mView = new QGraphicsView (mWidgetMain);
    mScene = new TransparentScene (mView);
    mView->setScene (mScene);

    // Reduce repaints when resizing and enable mouse tracking
    mView->viewport()->setAttribute (Qt::WA_StaticContents);
    mView->viewport()->setMouseTracking (true);

    // Don't display any scroll bars
    mView->setVerticalScrollBarPolicy   (Qt::ScrollBarAlwaysOff);
    mView->setHorizontalScrollBarPolicy (Qt::ScrollBarAlwaysOff);

    // Add the graphics view to the layout
    mLayoutMain = new QGridLayout (mWidgetMain);
    mLayoutMain->setSpacing (0);
    mLayoutMain->setContentsMargins (0, 0, 0, 0);
    mLayoutMain->addWidget (mView, 0, 0, 1, 1);

    // Make the background transparent
    setAttribute (Qt::WA_TranslucentBackground);



    setStyleSheet
            ("									\
             QWidget						\
    {								\
             background: transparent;	\
}								\
\
QFrame							\
{								\
background: transparent;	\
border: none;				\
}								\
");

// Add the central widget to this window
//setCentralWidget (mWidgetMain);
realx = realy = 0;
}

TransparentWindow::TransparentWindow (QWidget *parent, int x, int y, int width, int height)
    : QWidget (parent, Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint)
{
    // Create a new central widget
    mWidgetMain = new QWidget (this);

    // Create a new graphics view and scene
    mView = new QGraphicsView (mWidgetMain);
    mScene = new TransparentScene (mView);
    mView->setScene (mScene);

    // Reduce repaints when resizing and enable mouse tracking
    mView->viewport()->setAttribute (Qt::WA_StaticContents);
    mView->viewport()->setMouseTracking (true);
    mView->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    mView->setDragMode(QGraphicsView::NoDrag);

    // Don't display any scroll bars
    mView->setVerticalScrollBarPolicy   (Qt::ScrollBarAlwaysOff);
    mView->setHorizontalScrollBarPolicy (Qt::ScrollBarAlwaysOff);

    // Add the graphics view to the layout
    mLayoutMain = new QGridLayout (mWidgetMain);
    mLayoutMain->setSpacing (0);
    mLayoutMain->setContentsMargins (0, 0, 0, 0);
    mLayoutMain->addWidget (mView, 0, 0, 1, 1);

    // Make the background transparent
    setAttribute (Qt::WA_TranslucentBackground);




    setStyleSheet
            ("									\
             QWidget						\
    {								\
             background: transparent;	\
            background-color: none      \
}								\
        \
        QFrame							\
    {								\
background: transparent;	\
border: none;				\
    }								\
    ");

    // Add the central widget to this window
    //setCentralWidget (mWidgetMain);
    realx = realy = 0;
}

////////////////////////////////////////////////////////////////////////////////
/// <summary> Deletes any unmanaged memory used by this window. </summary>

TransparentWindow::~TransparentWindow (void)
{
}

//----------------------------------------------------------------------------//
// Functions                                                TransparentWindow //
//----------------------------------------------------------------------------//

void TransparentWindow::setWid(int width)
{
    this->widthV = width;
}

void TransparentWindow::setHeig(int height)
{
    this->heightV = height;
}

QPoint TransparentWindow::getCoordinates()
{
    return QPoint((mScene->getPressedPoint().x() + mScene->getRelasedPoint().x()) / 2, (mScene->getPressedPoint().y()+mScene->getRelasedPoint().y())/2);
}


// DEBUG ONLY
void TransparentWindow::printAllItems()
{
    qDebug() << "There are " << mView->items() << " items";
}

//----------------------------------------------------------------------------//
// Events                                                   TransparentWindow //
//----------------------------------------------------------------------------//

////////////////////////////////////////////////////////////////////////////////
/// <summary> Invoked whenever this window has been resized. </summary>

void TransparentWindow::resizeEvent (QResizeEvent* event)
{

    // VIEW SIZE DEBUG
    QRectF testRect = mView->sceneRect();
    mView->updateSceneRect(testRect);
    qDebug() << "testRect: " << testRect;

    QRect vRect = mScene->returnVideoPlayerSize();
    qDebug() << "vRect in resizeEvent: " << vRect;
    mScene->setSceneRect(0,0,1020,500);
    getTransparentWindowView()->setSceneRect(mScene->sceneRect());
    qDebug() << "transparent scenerect: " << getTransparentWindowView()->sceneRect();
   // mScene->update(mScene->sceneRect());
    qDebug() << "mScene->sceneRect() in resizeEvent: " << mScene->sceneRect();


 //   QWidget::resizeEvent(event);
}

void TransparentWindow::setViewSize()
{

  //  QRect vRect = mScene->returnVideoPlayerSize();
 //   mScene->setSceneRect(0,0, vRect.width(), vRect.height());
  //  update(mScene->sceneRect().toRect());
}

void TransparentWindow::closeEvent(QCloseEvent *event)
{
    QWidget::closeEvent(event);
}



// loop through the list and return the current position
QPointF TransparentWindow::getObjectInSceneAtPosition(int position)
{
    return objectsInScene.at(position);
}

QPointF TransparentWindow::getObjectInSceneAtPosition(QPointF point)
{
    foreach(QPointF curPoint , objectsInScene)
    {
        if( curPoint == point )
        {
            return curPoint;
        }
    }
}


/// NEW CLASS ///
SortString::SortString()
{

}

bool SortString::operator ()(const DrawingItem *a, const DrawingItem *b)
{
    return a->itemName < b->itemName;
}
