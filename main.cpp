#include <QApplication>
#include <time.h>
#include "guitrace.h"

int main(int argc, char *argv[])
{
    srand( time(NULL) );
    QApplication a(argc, argv);
    GUITrace w;
    w.show();
    
    return a.exec();
}
