#include "drawingitem.h"

#include <QDebug>

DrawingItem::DrawingItem(const QString &name, qreal posX, qreal posY, int circle, const QColor color)
{
    //this->color = Qt::blue;
    this->color = color;
    this->setPos(posX, posY);
    positionX = posX;
    positionY = posY;
    setFlags(QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable | QGraphicsItem::ItemSendsGeometryChanges );
    setZValue(1);
    setAcceptsHoverEvents(true);
    this->itemName = name;
    this->itemCoordinatesF.setX(posX);
    this->itemCoordinatesF.setY(posY);
    head = false;
    this->prevCoordinates = QPointF();
    this->headCoordinates = QPointF();
    this->circle = circle;
}



DrawingItem::~DrawingItem()
{

}


void DrawingItem::setItemCoordinates(QPoint point)
{
    this->itemCoordinates = point;
}

void DrawingItem::setItemCoordinatesF(QPointF point)
{
    this->prevCoordinates = this->itemCoordinatesF;
    this->itemCoordinatesF = point;
}

void DrawingItem::appendInCoordinatesList(qreal x, qreal y)
{
    QPointF list(x, y);
    this->coordinatesList.append(list);
}

void DrawingItem::setItemName(const QString &name)
{
    this->itemName = name;
}

void DrawingItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    qDebug() << "circle is: " << circle;
    //Q_UNUSED(widget);
    painter->setPen(this->color);
    painter->setBrush(QBrush(QColor(0,0,0,64)));
    painter->drawEllipse(0,0,circle,circle);
    /// TODO: KEEP WORKING FROM HERE !!!
    painter->drawText(0,0, itemName);
}

QPainterPath DrawingItem::shape() const
{
   QPainterPath path;
   path.addEllipse(0, 0, 20, 20);
   return path;
}

QRectF DrawingItem::boundingRect() const
{
    return QRectF(0, 0, 20,20);
}

