//----------------------------------------------------------------------------//
// Preambles                                                                  //
//----------------------------------------------------------------------------//

#ifndef TRANSPARENT_H
#define TRANSPARENT_H

class QWidget;
class QGraphicsView;
class QGridLayout;

#include <QGraphicsScene>
#include <QMainWindow>
#include <QDebug>
#include <QGraphicsItem>
#include <QMouseEvent>

#include "drawingitem.h"

//----------------------------------------------------------------------------//
// Classes                                                                    //
//----------------------------------------------------------------------------//

////////////////////////////////////////////////////////////////////////////////
/// <summary> </summary>

class TransparentScene : public QGraphicsScene
{
    Q_OBJECT

public:
	// Constructors
	TransparentScene (QObject* parent = NULL);
    QList<DrawingItem*> returnItemList() { return itemList; }
    void setList(  QList<DrawingItem*> list ) { itemList = list; }
    void removeFromList(int idx) { itemList.removeAt(idx); }
    QMap<QGraphicsItem*, DrawingItem*> returnMapList() { return mapList; }
    QString getCurrentDrawOption();
    int getCurrentDrawValue() { return value; }
    QRect returnVideoPlayerSize() { return videoPlayerSize; }
    QRectF returnSetVideoPlayerSize() { return setVideoPlayerSize; }
    int circle() { return circleSize; }

public slots:
    void DrawComboBoxSlot( const QString &text );
    void getDrawOrMove(int value);
    void setItemName(const QString &name);
    void getVideoPlayerSize(QRect point);
    QPointF getPressedPoint() { return pressedPoint; }
    QPointF getRelasedPoint() { return releasedPoint; }

    void setGender(const QString &name);
    void setHeadFoot(const QString &name);
    void getCircleSize(int size);

signals:
    void emitRadioButtonChangeSignal();

protected:
	// Internal
	virtual void drawBackground (QPainter* painter, const QRectF& rect);
	virtual void drawForeground (QPainter* painter, const QRectF& rect);


protected:
	// Events
    virtual void mousePressEvent		(QGraphicsSceneMouseEvent* event);
	virtual void mouseReleaseEvent		(QGraphicsSceneMouseEvent* event);
	virtual void mouseMoveEvent			(QGraphicsSceneMouseEvent* event);
	virtual void mouseDoubleClickEvent	(QGraphicsSceneMouseEvent* event);
    //virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value);

protected:
	// Fields


private:
    void setDrawToMove();
    void drawLineTo(const QPoint &endPoint);
    QGraphicsItem *item;
    QPainter    *painter;

    QPointF pressedPoint;
    QPointF releasedPoint;
    QList<DrawingItem*> itemList;
    QMap<QGraphicsItem*, DrawingItem*> mapList;
    QString drawOption;
    bool pressed;
    bool draw, move;
    int value;
    int incrementValue;
    int circleSize;
    QString itemName;
    QString gender;
    QString headOrFoot;
    QRect videoPlayerSize;
    QRectF setVideoPlayerSize;
};

////////////////////////////////////////////////////////////////////////////////
/// <summary> </summary>

class TransparentWindow : public QWidget
{
    Q_OBJECT

public:
	// Constructors
	 TransparentWindow (QWidget *parent = NULL);
     TransparentWindow (QWidget *parent = NULL, int x = 0, int y=0, int width=0, int height=0);
    ~TransparentWindow (void);

     TransparentScene *transparentScene() { return mScene; }

     void setWid(int width);
     void setHeig(int height);
     QPoint getCoordinates();
     QGraphicsView *getTransparentWindowView() { return mView; }
     QList<QPointF> getObjectsInScene() { return objectsInScene; }
     QPointF getObjectInSceneAtPosition(int position);
     QPointF getObjectInSceneAtPosition(QPointF point);
     void setViewSize();

     // DEBUG FUNCTIONS
     void printAllItems();
protected:
	// Events
     virtual void resizeEvent (QResizeEvent* event);

     virtual void closeEvent(QCloseEvent *event);

protected:
	// Fields
	QGraphicsView*		mView;
	TransparentScene*	mScene;

	QWidget*			mWidgetMain;
	QGridLayout*		mLayoutMain;

    QPointF pressedPoint;
    QPointF releasedPoint;

private:
    void drawLineTo(const QPoint &endPoint);
    int x, y, widthV, heightV;
    bool pressed;
    QList<QPointF> objectsInScene;

    qreal realx, realy;

};

class SortString
{
public:
    SortString();
    bool operator() (const DrawingItem *a, const DrawingItem *b);
};

#endif // TRANSPARENT_H
