#ifndef SAVE_H
#define SAVE_H

#include <QString>
#include <QPoint>
#include <QSettings>
#include <QDebug>
#include <QFile>
#include <QTextCodec>
#include <QStringList>

class Save
{
public:
    Save();
    void saveInformation( const QString &time, const QString &objectName, const QString &gender, const QString &headorfoot, QString &x, QString &y, QString &headC);
    void saveInformation( const QString &objectName, QList<QPoint> coordinates );
    void loadInformation( const QString &fileName); // implement that later
    void saveOldCoordinates(QString &x, QString &y);
    QPoint getOldCoordinates() { return oldCoordinates; }

private:
    QString settingFile;
    QString number;
    QPoint oldCoordinates;
};

#endif // SAVE_H
